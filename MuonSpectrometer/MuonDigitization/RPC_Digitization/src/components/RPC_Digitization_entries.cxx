/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "RPC_Digitization/RpcDigitizationTool.h"

DECLARE_COMPONENT(RpcDigitizationTool)
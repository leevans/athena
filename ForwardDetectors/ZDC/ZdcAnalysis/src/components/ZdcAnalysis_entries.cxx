#ifndef XAOD_STANDALONE

#include "ZdcAnalysis/ZdcAnalysisTool.h"
#include "ZdcAnalysis/RPDAnalysisTool.h"
#include "ZdcAnalysis/RpdSubtractCentroidTool.h"
#include "ZdcAnalysis/ZdcTrigValidTool.h"
#include "ZdcAnalysis/ZdcLEDAnalysisTool.h"

DECLARE_COMPONENT( ZDC::ZdcAnalysisTool )
DECLARE_COMPONENT( ZDC::RPDAnalysisTool )
DECLARE_COMPONENT( ZDC::RpdSubtractCentroidTool )
DECLARE_COMPONENT( ZDC::ZdcTrigValidTool )
DECLARE_COMPONENT( ZDC::ZdcLEDAnalysisTool )

#endif


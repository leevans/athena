#include "../TransportTool.h"
#include "../McMaterialEffectsUpdator.h"
#include "../HadIntProcessorParametric.h"
#include "../McEnergyLossUpdator.h"
#include "../PhotonConversionTool.h"
#include "../ProcessSamplingTool.h"
#include "../PhysicsValidationTool.h"

DECLARE_COMPONENT( iFatras::TransportTool )
DECLARE_COMPONENT( iFatras::McMaterialEffectsUpdator )
DECLARE_COMPONENT( iFatras::HadIntProcessorParametric )
DECLARE_COMPONENT( iFatras::McEnergyLossUpdator )
DECLARE_COMPONENT( iFatras::PhotonConversionTool )
DECLARE_COMPONENT( iFatras::ProcessSamplingTool )
DECLARE_COMPONENT( iFatras::PhysicsValidationTool )

